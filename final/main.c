// Jessica Baron	Spring 2017	CPSC 6050	Project 3
#include <stdio.h>
#include <stdlib.h>	//malloc(), calloc(), rand()
#include <fcntl.h>	//open(), read(), write(), close()
#include <string.h>	//for atoi() with fgets()
#include <math.h>	//for trig operations and M_PI constant
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

struct point
{ float x, y, z; };
struct vec4 
{ float x, y, z, w; };
struct mat3 
{ struct point vec0, vec1, vec2; };
struct mat4 
{ struct vec4 vec0, vec1, vec2, vec3; };
struct ray
{ struct point startPt, dir; };
struct triangle			//Used for intersection calculations.
{ struct point v0, v1, v2, n; };

//Box faces
struct point boxVerts[6][4] = 
{
	{{-1.0,-1.0,-1.0},{-1.0,1.0,-1.0},{1.0,1.0,-1.0},{1.0,-1.0,-1.0}},	//Back
	{{-1.0,-1.0,-1.0},{-1.0,-1.0,1.0},{-1.0,1.0,1.0},{-1.0,1.0,-1.0}},	//Left
	{{1.0,-1.0,-1.0},{1.0,1.0,-1.0},{1.0,1.0,1.0},{1.0,-1.0,1.0}},		//Right
	{{-1.0,1.0,-1.0},{-1.0,1.0,1.0},{1.0,1.0,1.0},{1.0,1.0,-1.0}},		//Top
	{{-1.0,-1.0,-1.0},{-1.0,-1.0,1.0},{1.0,-1.0,1.0},{1.0,-1.0,-1.0}},	//Bottom
	{{-1.0,-1.0,1.0},{-1.0,1.0,1.0},{1.0,1.0,1.0},{1.0,-1.0,1.0}}		//Front
};
struct point boxNorms[6] = 
{
	{0.0,0.0,1.0},	//Back	
	{1.0,0.0,0.0},	//Left
	{-1.0,0.0,0.0},	//Right
	{0.0,-1.0,0.0},	//Top
	{0.0,1.0,0.0},	//Bottom
	{0.0,0.0,-1.0}	//Front
};
struct point boxColors[6] =
{
	{0.95, 0.94, 0.93},	//Off-white for back	
	{0.7, 0.3, 0.8},	//Purple for right
	{0.0, 0.7, 0.6},	//Teal for left
	{0.95, 0.94, 0.93},	//White for top 
	{0.95, 0.94, 0.93},	//White for bottom 
	{0.95, 0.94, 0.93}	//White for front 
};

struct point eye = {-0.2, -0.45, 2.5},   view = {0.0, -0.05, 0.0};	//Up calculated when view volume is set.
struct point lightSrc[4] = {{-0.2,0.9,-0.2},{-0.2,0.9,0.2},{0.2,0.9,0.2},{0.2,0.9,-0.2}};
struct point origLightDiffuse = {2.0, 2.0, 2.0};			//Initial light color is white.
struct point origLightSpecular = {1.0, 1.0, 1.0};			//The light settings are global since setLights() is called by different functions.
struct point lightDiffuse = {1.0, 1.0, 1.0};
struct point lightSpecular = {1.0, 1.0, 1.0};
struct point lightPos = {0.0, 0.9, 0.0};   
struct triangle boxTris[12];  						//Twice the number of box quads.
struct triangle *teapotTris; 
struct point teapotMaterials[3];
struct point teaTrans = {0.6, -0.8, -0.6};
float teapotShiny, teaScale = 0.6, teaRotateX = -40.0, teaRotateY = 120.0, teaRotateZ = 10.0; 
GLuint normalMap_ID=1, shadowMap_ID=2;

int numVerts, numNormals, numTexCoords, numFaces, boxShaderProg, teapotShaderProg, lightSrcShaderProg;
int WINW = 1280, WINH = 1024; 
int DM_W = 512, DM_H = 512; 	//Depth (shadow) map dimensions.
float JITTER = 0.004;		//For antialiasing.
GLfloat *vertices, *texCoords, *normals, *texCoords;
GLfloat *finalVertices, *finalNormals, *finalTangents, *finalBitangents, *finalTexCoords;	
GLuint *faceTexCoords, *faceNormals, *faceVerts;

//------------------------------------------------------------------
void printPoint(struct point p)
{ printf("{%f,%f,%f}\n", p.x, p.y, p.z); }

void printVec4(struct vec4 v)
{ printf("{%f,%f,%f,%f}\n", v.x, v.y, v.z, v.w); }

//-------------------------------------------------------------------------
//Linear algebra functions. Matrices are assumed to be column-major.
double dotProduct(struct point a, struct point b)
{ return (a.x * b.x) + (a.y * b.y) + (a.z * b.z); }
	
double dotProduct4(struct vec4 a, struct vec4 b)
{ return (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w); }
	
struct point crossProduct(struct point a, struct point b)
{
	struct point ans;
        ans.x = (a.y * b.z) - (a.z * b.y);
        ans.y = (a.z * b.x) - (a.x * b.z);
        ans.z = (a.x * b.y) - (a.y * b.x);
	return ans;
}

struct point componentMult(struct point a, struct point b)
{
	struct point ans;
	ans.x = a.x * b.x;
	ans.y = a.y * b.y;
	ans.z = a.z * b.z;
	return ans;
}

struct point scaleVec(struct point v, double n)
{
	struct point ans;
        ans.x = v.x * n;
        ans.y = v.y * n;
        ans.z = v.z * n;
	return ans;
}

struct point addVecs(struct point a, struct point b)
{
	struct point ans;
	ans.x = a.x + b.x;
	ans.y = a.y + b.y;
	ans.z = a.z + b.z;
	return ans;
}

struct point multMat3ByVec(struct mat3 M, struct point v)
{
	struct point ans;
	ans.x = dotProduct(M.vec0, v);
	ans.y = dotProduct(M.vec1, v);
	ans.z = dotProduct(M.vec2, v);
	return ans;
}

struct vec4 multMat4ByVec4(struct mat4 M, struct vec4 v)
{
	struct vec4 ans;
	ans.x = dotProduct4(M.vec0, v);
	ans.y = dotProduct4(M.vec1, v);
	ans.z = dotProduct4(M.vec2, v);
	ans.w = dotProduct4(M.vec3, v);
	return ans;
}

struct mat4 multMat4ByMat4(struct mat4 M, struct mat4 N)
{
	struct mat4 ans;
	ans.vec0.x = M.vec0.x*N.vec0.x + M.vec0.y*N.vec1.x + M.vec0.z*N.vec2.x + M.vec0.w*N.vec3.x;
	ans.vec0.y = M.vec0.x*N.vec0.y + M.vec0.y*N.vec1.y + M.vec0.z*N.vec2.y + M.vec0.w*N.vec3.y;
	ans.vec0.z = M.vec0.x*N.vec0.z + M.vec0.y*N.vec1.z + M.vec0.z*N.vec2.z + M.vec0.w*N.vec3.w;
	ans.vec0.w = M.vec0.x*N.vec0.w + M.vec0.y*N.vec1.w + M.vec0.z*N.vec2.w + M.vec0.w*N.vec3.z;
	
	ans.vec1.x = M.vec1.x*N.vec0.x + M.vec1.y*N.vec1.x + M.vec1.z*N.vec2.x + M.vec1.w*N.vec3.x;
	ans.vec1.y = M.vec1.x*N.vec0.y + M.vec1.y*N.vec1.y + M.vec1.z*N.vec2.y + M.vec1.w*N.vec3.y;
	ans.vec1.z = M.vec1.x*N.vec0.z + M.vec1.y*N.vec1.z + M.vec1.z*N.vec2.z + M.vec1.w*N.vec3.w;
	ans.vec1.w = M.vec1.x*N.vec0.w + M.vec1.y*N.vec1.w + M.vec1.z*N.vec2.w + M.vec1.w*N.vec3.z;

	ans.vec2.x = M.vec2.x*N.vec0.x + M.vec2.y*N.vec1.x + M.vec2.z*N.vec2.x + M.vec2.w*N.vec3.x;
	ans.vec2.y = M.vec2.x*N.vec0.y + M.vec2.y*N.vec1.y + M.vec2.z*N.vec2.y + M.vec2.w*N.vec3.y;
	ans.vec2.z = M.vec2.x*N.vec0.z + M.vec2.y*N.vec1.z + M.vec2.z*N.vec2.z + M.vec2.w*N.vec3.w;
	ans.vec2.w = M.vec2.x*N.vec0.w + M.vec2.y*N.vec1.w + M.vec2.z*N.vec2.w + M.vec2.w*N.vec3.z;

	ans.vec3.x = M.vec3.x*N.vec0.x + M.vec3.y*N.vec1.x + M.vec3.z*N.vec2.x + M.vec3.w*N.vec3.x;
	ans.vec3.y = M.vec3.x*N.vec0.y + M.vec3.y*N.vec1.y + M.vec3.z*N.vec2.y + M.vec3.w*N.vec3.y;
	ans.vec3.z = M.vec3.x*N.vec0.z + M.vec3.y*N.vec1.z + M.vec3.z*N.vec2.z + M.vec3.w*N.vec3.w;
	ans.vec3.w = M.vec3.x*N.vec0.w + M.vec3.y*N.vec1.w + M.vec3.z*N.vec2.w + M.vec3.w*N.vec3.z;

	return ans;	
}

//Column-major matrices
struct mat4 getRotateXMat(float angle)
{
	struct mat4 M =
	{ 
		{1.0, 0.0, 0.0, 0.0},
		{0.0, cos(angle), sin(angle), 0.0},
		{0.0, -sin(angle), cos(angle), 0.0},
		{0.0, 0.0, 0.0, 1.0}
	};

	return M;
}

struct mat4 getRotateYMat(float angle)
{
	struct mat4 M =
	{ 
		{cos(angle), 0.0, -sin(angle), 0.0},
		{0.0, 1.0, 0.0, 0.0},
		{sin(angle), 0.0, cos(angle), 0.0},
		{0.0, 0.0, 0.0, 1.0}
	};

	return M;
}

struct mat4 getRotateZMat(float angle)
{
	struct mat4 M =
	{ 
		{cos(angle), sin(angle), 0.0, 0.0},
		{-sin(angle), cos(angle), 0.0, 0.0},
		{0.0, 0.0, 1.0, 0.0},
		{0.0, 0.0, 0.0, 1.0}
	};

	return M;
}

struct mat4 getTranslMat(struct point transl)
{
	struct mat4 M =
	{
		{1.0, 0.0, 0.0, transl.x},
		{0.0, 1.0, 0.0, transl.y},
		{0.0, 0.0, 1.0, transl.z},
		{0.0, 0.0, 0.0, 1.0}
	};

	return M;
}

struct mat3 inverseTranspose(struct mat3 M)
{
	float det;
	struct mat3 ans;	
	det = dotProduct(M.vec0, crossProduct(M.vec1, M.vec2));
	ans.vec0 = scaleVec(crossProduct(M.vec1, M.vec2), 1/det);
	ans.vec1 = scaleVec(crossProduct( scaleVec(M.vec0, -1.0), M.vec2), 1/det);
	ans.vec2 = scaleVec(crossProduct(M.vec0, M.vec1), 1/det);
	return ans;
}

struct point normalize(struct point v)
{
	struct point ans;
	float mag = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	ans.x = v.x / mag;
	ans.y = v.y / mag;
	ans.z = v.z / mag;
	return ans;
}

//-------------------------------------------------------------------------
void loadTexture(char *filename, unsigned int texID)
{
        FILE *fopen(), *fptr;
        char buf[512];
        int imgSize, imgWidth, imgHeight, maxColor, face;
        unsigned char *texBytes, *parse;

        fptr=fopen(filename,"r");
        fgets(buf,512,fptr);    //Strip first line.

        do                      //Strip off comments.
        { fgets(buf,512,fptr); }
        while(buf[0]=='#');

        parse = strtok(buf," \t");
        imgWidth = atoi(parse);
        parse = strtok(NULL," \n");
        imgHeight = atoi(parse);

        fgets(buf,512,fptr);
        parse = strtok(buf," \n");
        maxColor = atoi(parse);

        imgSize = imgWidth*imgHeight;
        texBytes = (unsigned char *)calloc(3,imgSize);
        fread(texBytes,3,imgSize,fptr);
        fclose(fptr);

        glBindTexture(GL_TEXTURE_2D, texID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgWidth, imgHeight, 
		0, GL_RGB, GL_UNSIGNED_BYTE, texBytes);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);         //Texture magnification with small (>=1) area of texture element.
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        free(texBytes);
	glBindTexture(GL_TEXTURE_2D, 0);
}

//------------------------------------------------------------------
//Reads the content of a file byte by byte. Used in compiling the shader programs.
char* readShader(char *filename)
{
        FILE *filePointer;
        char *content = NULL;
        int fileDescr, fileSize;   
        fileDescr = open(filename, O_RDONLY);           //Descriptor indexes into the file.
        fileSize = lseek(fileDescr, 0, SEEK_END);       //Seek to end of file. Size is in bytes.
        content = (char *) calloc(1,(fileSize+1));      //Allocate space as bytes needed plus one for the null char at end of string.
        lseek(fileDescr, 0, SEEK_SET);                  //Set descriptor to the front of the file again, preparing to read.
        fileSize = read(fileDescr, content, fileSize*sizeof(char));	//Returns the number of bytes read. (If this value is 0, something went wrong.)
        content[fileSize] = '\0';                       //Null character at end of string.
        close(fileDescr);
        return content;
}

//------------------------------------------------------------------
//Read, compile, and link vertex and fragment shader programs.
//Returns the OpenGL program linked to the shaders.
unsigned int setShaders(char *vertFile, char *fragFile)
{
        GLuint vertCompiled, fragCompiled, vertShader, fragShader, prog;
        char *vertContent, *fragContent;
        vertContent = readShader(vertFile);
        fragContent = readShader(fragFile);

        vertShader = glCreateShader(GL_VERTEX_SHADER);          //returns an int
        fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    //1 = length of string array, vert content. Load the content into the shader via pointer to a char array (pointer). NULL means all strings end in null char ('\0').
        glShaderSource(vertShader, 1, (const char **)&vertContent, NULL);
        glShaderSource(fragShader, 1, (const char **)&fragContent, NULL);
        free(vertContent);      //Not using original read content anymore.
        free(fragContent);
        glCompileShader(vertShader);
        glCompileShader(fragShader);
        prog = glCreateProgram();
        glAttachShader(prog, vertShader);
        glAttachShader(prog, fragShader);
        glLinkProgram(prog);
        return(prog);
}

//------------------------------------------------------------------
//Parse MTL file associated with an OBJ file. Called by parseObj().
void parseMtl(char *filename)
{
	FILE *fptr;
	char *parse;
	float shiny;
	int lineLength = 512;	
	char buf[lineLength];	
	struct point amb, dif, spec;
	fptr = fopen(filename, "r");	if (!fptr) printf("Could not load file %s as mtl.\n", filename);
	while ( fgets(buf, lineLength, fptr) )		
	{
		if (buf[0] == 'K')
		{
			if (buf[1] == 'a')			//Material ambient.
			{
				parse = strtok(buf, " \t");	//Strip Ka.
				parse = strtok(NULL, " \t");
				amb.x = atof(parse);
				parse = strtok(NULL, " \t");
				amb.y = atof(parse);
				parse = strtok(NULL, " \t\n");
				amb.z = atof(parse);
			}
			else if (buf[1] == 'd')			//Material diffuse.
			{
				parse = strtok(buf, " \t");	//Strip Kd.
				parse = strtok(NULL, " \t");
				dif.x = atof(parse);
				parse = strtok(NULL, " \t");
				dif.y = atof(parse);
				parse = strtok(NULL, " \t\n");
				dif.z = atof(parse);
			}
			else if (buf[1] == 's')			//Material specular.
			{
				parse = strtok(buf, " \t");
				parse = strtok(NULL, " \t");	//Strip Ks.
				spec.x = atof(parse);
				parse = strtok(NULL, " \t");
				spec.y = atof(parse);
				parse = strtok(NULL, " \t\n");
				spec.z = atof(parse);
			}
		}
		else if (buf[0] == 'N')
		{
			parse = strtok(buf, " ");	//Strip Ns.
			parse = strtok(NULL, " \t\n");	
			shiny = atof(parse);	
		}
		else if (buf[0] == 'm')
		{
			parse = strtok(buf, "_");	//Strip map.
			parse = strtok(NULL, " \t");	//Should be "Kd" or "normal".
			if (parse[0] == 'n')		//Normal map.
			{
				parse = strtok(NULL, " \t\n");	//Filename
				printf("Normal map: %s\n", parse);
				loadTexture(parse, normalMap_ID);	
			}
		}
	}
	teapotMaterials[0] = amb;
	teapotMaterials[1] = dif;
	teapotMaterials[2] = spec;
	teapotShiny = shiny;
}

//------------------------------------------------------------------
//Parses an OBJ file and updates global geometry arrays (vertices, normals, etc.).
void parseObj(char *filename)
{
	FILE *fptr;
	char *parse;
	int lineLength = 1024, numTangents = 0, numBitangents = 0, i;
	int vertInd = 0, texCoordInd = 0, normalInd = 0, tangentInd = 0, bitangentInd = 0, faceInd = 0;
	char buf[lineLength];
	GLfloat *tangents, *bitangents;

	fptr = fopen(filename, "r");	if (!fptr) printf("Could not load file %s as OBJ.\n", filename);
	while ( fgets(buf, lineLength, fptr) )		//First get all counts.
	{
		if (buf[0] == 'v')	
		{	
			if (buf[1] == ' ') 	numVerts++; 
			else if (buf[1] == 't')	numTexCoords++; 
			else if (buf[1] == 'n') numNormals++; 	
			else if (buf[1] == 'x') numTangents++; 
			else if (buf[1] == 'y') numBitangents++; 
		}	
		else if (buf[0] == 'f')	numFaces++;
	}	
	rewind(fptr);			//Return pointer to beginning of file.
	
	vertices = (GLfloat *) malloc(numVerts * 3 * sizeof(GLfloat));
	texCoords = (GLfloat *) malloc(numTexCoords * 2 * sizeof(GLfloat));
	normals = (GLfloat *) malloc(numNormals * 3 * sizeof(GLfloat));
	tangents = (GLfloat *) malloc(numTangents * 3 * sizeof(GLfloat));
	bitangents = (GLfloat *) malloc(numBitangents * 3 * sizeof(GLfloat));

	faceVerts = (GLuint*) malloc(numFaces * 4 * sizeof(GLuint));		//This saves just vertex indices. 
	faceTexCoords = (GLuint*) malloc(numFaces * 4 * sizeof(GLuint));	//Faces also have vt and vn info.	
	faceNormals = (GLuint*) malloc(numFaces * 4 * sizeof(GLuint));	
	teapotTris = (struct triangle*) malloc(2 * numFaces * sizeof(struct triangle));	//Number of triangles is twice the number of quad faces. Needed for ray intersection calculations.

	while ( fgets(buf, lineLength, fptr) )		
	{
		if (buf[0] == 'v')	
		{	
			if (buf[1] == ' ') 
			{
				parse = strtok(buf, " \t");	//Strip 'v'.
				parse = strtok(NULL, " \t");	//x-coord
				vertices[vertInd] = atof(parse); 
				parse = strtok(NULL, " \t");	//y-coord
				vertices[vertInd+1] = atof(parse); 
				parse = strtok(NULL, " \t\n");	//z-coord
				vertices[vertInd+2] = atof(parse); 
				
				vertInd += 3;			//3 coords per vertex
			}	
			else if (buf[1] == 't') 
			{
				parse = strtok(buf, " \t");	//Strip 'vt'.
				parse = strtok(NULL, " \t");	//s-coord
				texCoords[texCoordInd] = atof(parse);
				parse = strtok(NULL, " \t\n");	//t-coord
				texCoords[texCoordInd+1] = atof(parse);
				texCoordInd += 2; 		//2 coords per tex coord	
			}	
			else if (buf[1] == 'n') 
			{
				parse = strtok(buf, " \t");	//Strip 'vn'.
				parse = strtok(NULL, " \t");	//x-coord
				normals[normalInd] = atof(parse); 
				parse = strtok(NULL, " \t");	//y-coord
				normals[normalInd+1] = atof(parse); 
				parse = strtok(NULL, " \t\n");	//z-coord
				normals[normalInd+2] = atof(parse); 
				normalInd += 3;			//3 coords per normal 
			}	
			else if (buf[1] == 'x')
			{
				parse = strtok(buf, " \t");	//Strip 'vx'.
				parse = strtok(NULL, " \t");	//x-coord
				tangents[tangentInd] = atof(parse); 
				parse = strtok(NULL, " \t");	//y-coord
				tangents[tangentInd+1] = atof(parse); 
				parse = strtok(NULL, " \t\n");	//z-coord
				tangents[tangentInd+2] = atof(parse); 
				tangentInd += 3;		//3 coords per tangent vector 
			}	
			else if (buf[1] == 'y')
			{
				parse = strtok(buf, " \t");	//Strip 'vy'.
				parse = strtok(NULL, " \t");	//x-coord
				bitangents[bitangentInd] = atof(parse); 
				parse = strtok(NULL, " \t");	//y-coord
				bitangents[bitangentInd+1] = atof(parse); 
				parse = strtok(NULL, " \t\n");	//z-coord
				bitangents[bitangentInd+2] = atof(parse); 
				bitangentInd += 3;		//3 coords per bitangent vector 
			}	
		}	
			
		else if (buf[0] == 'f')
		{
			parse = strtok(buf, " \t");		//Strip 'f'.
			for (i=0; i<4; i++)
			{
				parse = strtok(NULL, "/");			//Vertex index of i-th face vertex
				faceVerts[faceInd+i] = atoi(parse)-1;		//-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
				parse = strtok(NULL, "/");			//Tex coords index of i-th face vertex
				faceTexCoords[faceInd+i] = atoi(parse)-1;	//-1!
				if (i==3) 	
				{ parse = strtok(NULL, " \t\n"); }		//Normal index of LAST face vertex (at end of line)
				else 		
				{ parse = strtok(NULL, " \t"); }		//Normal index of i-th face vertex
				faceNormals[faceInd+i] = atoi(parse) -1;		//-1
			}
			faceInd += 4;
			
		}	
		else if (buf[0] == 'm')			//"mtllib <filename>"	
		{	
			parse = strtok(buf, " \t");	//Strip "mtllib".
			parse = strtok(NULL, " \t\n");	//The filename	
			parseMtl(parse);	
		}	
	}	

	fclose(fptr);

	printf("Num verts: %d \nNum faces: %d \nNum texcoords %d \nNum normals %d \nNum tangents %d \nNum bitangents %d \n", 
		numVerts, numFaces, numTexCoords, numNormals, numTangents, numBitangents);

   //Approach without GL element arrays. (DrawElements does not allow different indices for vertices and normals.)
	finalVertices = (GLfloat *) malloc(4 * numFaces * 3 * sizeof(GLfloat));	//4 verts per face * components (3 or 2) per item
	finalNormals = (GLfloat *) malloc(4 * numFaces * 3 * sizeof(GLfloat));
	finalTangents = (GLfloat *) malloc(4 * numFaces * 3 * sizeof(GLfloat));
	finalBitangents = (GLfloat *) malloc(4 * numFaces * 3 * sizeof(GLfloat));
	finalTexCoords = (GLfloat *) malloc(4 * numFaces * 2 * sizeof(GLfloat));
	
	int fVert, indV, indN, indTex;
	for(fVert=0; fVert<numFaces*4; fVert++)
	{
		indV = faceVerts[fVert];	
		indN = faceNormals[fVert];	
		indTex = faceTexCoords[fVert];	
		for (i=0; i<3; i++)
		{
			finalVertices[fVert*3+i] = vertices[indV*3+i];
			finalNormals[fVert*3+i] = normals[indN*3+i];

			finalTangents[fVert*3+i] = tangents[indV*3+i];		//Tangents and bitangents ordered by VERTEX index.
			finalBitangents[fVert*3+i] = bitangents[indV*3+i];
		}
		for (i=0; i<2; i++)
		{
			finalTexCoords[fVert*2+i] = texCoords[indTex*2+i];
		}
	}	
	
	free(vertices);	
	free(normals);
	free(tangents);
	free(bitangents);
	free(texCoords);	
	free(faceNormals);	
	free(faceTexCoords);	
}

//-------------------------------------------------------------------------
//Populate the boxTris array with tri structs based on the box quad faces. Used when checking ray-triangle intersections.
void getBoxTris()
{
	int tri;
   //Split each quad {v0,v1,v2,v3} into two triangles: {v0,v1,v2,n}, {v0,v2,v3,n}
	for (tri=0; tri<12; tri+=2)	//Each box face.
	{		
	// First tri from quad: {v0,v1,v2,n}		
		boxTris[tri].v0 = boxVerts[tri/2][0];	//Corresponding face index of a tri is half the tri index.
		boxTris[tri].v1 = boxVerts[tri/2][1];
		boxTris[tri].v2 = boxVerts[tri/2][2];
		boxTris[tri].n = boxNorms[tri/2];
			
	// 2nd tri from quad: {v0,v2,v3,n}		
		boxTris[tri+1].v0 = boxVerts[tri/2][0];
		boxTris[tri+1].v1 = boxVerts[tri/2][2];
		boxTris[tri+1].v2 = boxVerts[tri/2][3];
		boxTris[tri+1].n = boxNorms[tri/2];
	}
}

//-------------------------------------------------------------------------
//Calculate an up vector based on the given eye and view point. Adapted from Dr. Geist's shadow accumulation example.
struct point getUpVec(struct point eyePt, struct point viewPt)
{
// "Calculate up vector as unique vector u satisfying
// u = Av + B(0,1,0), u o v = 0, ||u|| = 1, u.y > 0
// where v denotes unit-length view vector."
	double A, B;
	struct point viewVector, upVector;
	viewVector.x = viewPt.x - eyePt.x;
	viewVector.y = viewPt.y - eyePt.y;
	viewVector.z = viewPt.z - eyePt.z;
	viewVector = normalize(viewVector);

	if (((int)viewVector.y == 1) || ((int)viewVector.y == -1))	//This would cause division by 0.
	{ viewVector.y += 0.00001; }

	A = -viewVector.y / sqrt(1.0 - viewVector.y*viewVector.y);
	B = 1.0 / sqrt(1.0 - viewVector.y*viewVector.y);

	upVector.x = A*viewVector.x;
	upVector.y = A*viewVector.y + B;
	upVector.z = A*viewVector.z;
	
	return upVector;
}

//-------------------------------------------------------------------------
double genrand()
{ return(((double)(random()+1))/2147483649.); }

//-------------------------------------------------------------------------
//Set up the initial projection and modelview matrices of the scene.
void setupViewVolume(int w, int h, struct point eyePt, struct point viewPt)
{
        glMatrixMode(GL_PROJECTION);     	       		        //Set current matrix as projection matrix.
        glLoadIdentity();                      		       		//Set proj mat as the identity matrix.
	gluPerspective(45.0,(float)(w)/(float)(h), 1.0, 20.0); 		//45 degree FOV, aspect ratio=width/height, near CP=0.1, farCP=20

        glMatrixMode(GL_MODELVIEW);                     		//Now set ModelView mat.
        glLoadIdentity();

	struct point upVector;
	upVector = getUpVec(eyePt, viewPt);
        gluLookAt(eyePt.x,eyePt.y,eyePt.z,viewPt.x,viewPt.y,viewPt.z,upVector.x,upVector.y,upVector.z);
}
	
//-------------------------------------------------------------------------
//Initialize texture and bind bytes from off-screen framebuffer.
void buildShadowmap()	
{
	glBindTexture(GL_TEXTURE_2D, shadowMap_ID); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, DM_W, DM_H, 0, 		//Internal depth component value. This texture will get data later (initially no data, 0 as last param).
        	GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	
	glBindTexture(GL_TEXTURE_2D, 0); 		
	
	glBindFramebufferEXT(GL_FRAMEBUFFER,1);								//Offscreen buffer.
	glDrawBuffer(GL_NONE); 										//No color buffers will be written.	
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowMap_ID, 0);	//Bind depth info in framebuffer to tex ID (2nd to last arg).
													//	Allows offscreen rend-to-texture.
	glBindFramebufferEXT(GL_FRAMEBUFFER,0);								//Visible default framebuffer.
}
	
//------------------------------------------------------------------
//Phi function for the Halton Sequence.	
float haltonPhi(int b, int i)
{
	float x, f;
	x = 0;	
	f = 1.0 / (float) b;
	while (i)
	{
		x += (f * (float)(i%b));
		i /= b;
		f *= 1.0 / (float) b;	
	}
	return x;	
}	
	
//------------------------------------------------------------------
//Generate a direction on the unit sphere with ind as the current ray index.
struct point getDirection(int ind)	
{
	struct point dir;	
	float az, el;			//Azimuth and elevation of point on unit sphere.
	int negY=1;			//Randomly determine if y negated (should be most of the time). 
	if ((rand()%10) < 9) negY = -1;	//95% chance to be negated.
	
	az = 2*M_PI * haltonPhi(2, ind);
	el = asin( haltonPhi(3, ind) );
	dir.x = -sin(az) * cos(el);	
	dir.y = negY * sin(el);
	dir.z = cos(az) * cos(el);	
	
	return dir;	
}	

//-------------------------------------------------------------------------
//Set current GL light to given diffuse, specular, and position values.
void setLights(struct point diffuse, struct point specular, struct point pos)
{
        float light_pos[] = {pos.x, pos.y, pos.z, 1.0}; 
        float light_diffuse[] = {diffuse.x, diffuse.y, diffuse.z, 0.0};
        float light_specular[] = {specular.x, specular.y, specular.z, 0.0};

        glMatrixMode(GL_MODELVIEW);                             //Make sure to set this so glLightfv() can create eye-space coords for the light positions.
        glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);

	glEnable(GL_LIGHT0);
}
	
//------------------------------------------------------------------
//ray = startPt*(1-t) + dir*t | t>=0 (if t=1 then at dir)
//ray = origin + t*dir (not dir)
void testDrawRay(struct ray r, struct point end)
{
	glUseProgram(0);
	int i, n=100;
	float t,x,y,z;
	glPointSize(20.0);
	glBegin(GL_POINTS);
	glVertex3f(r.startPt.x, r.startPt.y, r.startPt.z);	
	glEnd();
	
	glPointSize(3.0);
	glBegin(GL_POINTS);
	for (i=1; i<n; i++)
	{
		t = i/(1.0*n);
		x = r.startPt.x*(1-t) + end.x*t;	
		y = r.startPt.y*(1-t) + end.y*t;	
		z = r.startPt.z*(1-t) + end.z*t;	
		glVertex3f(x,y,z);	
	}
	glEnd();
}

//------------------------------------------------------------------
//Returns reflected ray when intersecting with a triangle in the scene. Otherwise, returns same ray.
struct ray castRay(struct ray r)	
{
   	int i, t, f, useTri0 = 0, useTri1 = 0;
    	struct point tuv0, tuv1, V, endPt;
    	struct mat3 M;	
    	struct ray newRay;	
    	struct triangle tri0, tri1;
	struct vec4 newPt;
	struct mat4 transf =		//Transformation matrix for the teapot. Since teapot geometry is transformed when rendering, check with those same transformations for intersections.
	{
		{teaScale, 0.0, 0.0, 0.0},
		{0.0, teaScale, 0.0, 0.0},
		{0.0, 0.0, teaScale, 0.0},
		{0.0, 0.0, 0.0, 1.0}
	};

	transf = multMat4ByMat4( getTranslMat(teaTrans), transf);
	transf = multMat4ByMat4( getRotateZMat(teaRotateZ), transf );
	transf = multMat4ByMat4( getRotateXMat(teaRotateX), transf );
	transf = multMat4ByMat4( getRotateYMat(teaRotateY), transf );	

    //ray: startPt*(1-t) + dir*t | t>=0 (if t=1 then at dir)
    //triangle: (1-u-v)*v0 + u*v1 + v*v2 | u,v >= 0 && u,v <= 1
  	for (i=0; i < (numFaces*2 +12); i+=2)
   	{			
            //Check teapot intersection first before walls.	
		if (i >= (numFaces*2))
		{ 
  			t = i - (numFaces*2); 
			tri0 = boxTris[t];
			tri0 = boxTris[t];
			tri1 = boxTris[t+1];
		}
            //Split each teapot quad {v0,v1,v2,v3} into two triangles: {v0,v1,v2,n}, {v0,v2,v3,n}
		else	
		{ 
  			f = i/2; 
		// First tri from quad: {v0,v1,v2,n}		
			newPt.x = vertices[faceVerts[f*4]];
			newPt.y = vertices[faceVerts[f*4] +1];
			newPt.z = vertices[faceVerts[f*4] +2];
			newPt.w = 1.0;	
			newPt = multMat4ByVec4(transf, newPt);		//Need 4D vector (with homogeneous coord) to transform.
			tri0.v0.x = newPt.x;				//Keep the 3D info to use in the triangle.
			tri0.v0.y = newPt.y;
			tri0.v0.z = newPt.z;
		
			newPt.x = vertices[faceVerts[f*4 +1]];
			newPt.y = vertices[faceVerts[f*4 +1] +1];
			newPt.z = vertices[faceVerts[f*4 +1] +2];
			newPt.w = 1.0;	
			newPt = multMat4ByVec4(transf, newPt);	
			tri0.v1.x = newPt.x;
			tri0.v1.y = newPt.y;
			tri0.v1.z = newPt.z;
			
			newPt.x = vertices[faceVerts[f*4 +2]];
			newPt.y = vertices[faceVerts[f*4 +2] +1];
			newPt.z = vertices[faceVerts[f*4 +2] +2];
			newPt.w = 1.0;	
			newPt = multMat4ByVec4(transf, newPt);	
			tri0.v2.x = newPt.x;
			tri0.v2.y = newPt.y;
			tri0.v2.z = newPt.z;
						//Calculate normal coming out of v1, orthogonal to v0-v1 and v2-v1.
			tri0.n = normalize(crossProduct( addVecs(tri0.v0, scaleVec(tri0.v1, -1)), addVecs(tri0.v2, scaleVec(tri0.v1, -1)) ));
			
		// 2nd tri from quad: {v0,v2,v3,n}		
			tri1.v0 = tri0.v0;				//Two vertices are shared with the first triangle.
			tri1.v1 = tri0.v2;	
			newPt.x = vertices[faceVerts[f*4 +3]]; 
			newPt.y = vertices[faceVerts[f*4 +3] +1]; 
			newPt.z = vertices[faceVerts[f*4 +3] +2]; 
			newPt.w = 1.0;	
			newPt = multMat4ByVec4(transf, newPt);	
			tri1.v2.x = newPt.x;
			tri1.v2.y = newPt.y;
			tri1.v2.z = newPt.z;
			tri1.n = tri0.n;
		}	
   		
		endPt = addVecs(r.startPt, r.dir);	//Actual end point (dp) is along start (startPt) in direction (dir) (start + dir).
		
	//Calculate t,u,v for intersection with tri0.	
		M.vec0 = addVecs(r.startPt, scaleVec(endPt, -1));		//sp - dp
		M.vec1 = addVecs(tri0.v1, scaleVec(tri0.v0, -1));		//tri vert1 - tri vert0 
		M.vec2 = addVecs(tri0.v2, scaleVec(tri0.v0, -1));		//tri vert2 - tri vert0 
		M = inverseTranspose(M);
		tuv0 = multMat3ByVec( M, addVecs(r.startPt, scaleVec(tri0.v0, -1)) );	//M * (startPt - tri vert0)

	//Calculate t,u,v for intersection with tri1.	
		M.vec0 = addVecs(r.startPt, scaleVec(endPt, -1));		//sp - dp
		M.vec1 = addVecs(tri1.v1, scaleVec(tri1.v0, -1));		//tri vert1 - tri vert0 
		M.vec2 = addVecs(tri1.v2, scaleVec(tri1.v0, -1));		//tri vert2 - tri vert0 
		M = inverseTranspose(M);
		tuv1 = multMat3ByVec( M, addVecs(r.startPt, scaleVec(tri1.v0, -1)) );	//M * (startPt - tri vert0)

	//t should be 0 or positive. u,v >= 0 and u+v <= 1.
		if ( (tuv0.x >= 0) && (tuv0.y >= 0) && (tuv0.z >= 0) && (tuv0.y + tuv0.z <= 1))
		{ useTri0 = 1; }
		else if ( (tuv1.x >= 0) && (tuv1.y >= 0) && (tuv1.z >= 0) && (tuv1.y + tuv1.z <= 1))
		{ useTri1 = 1; }
		
		if ((useTri0) || (useTri1))
		{
			V = scaleVec(r.dir, -1);				//When calculating reflection, starts at same point as normal.	
	
			if (i<(numFaces*2))
			{ printf("teapot tri\n"); }	
			else	
			{ printf("box tri\n"); }	
			
			if (useTri0)	
			{
				newRay.startPt = addVecs( scaleVec(r.startPt, 1-tuv0.x), scaleVec(endPt, tuv0.x) );		//sp*(1-t) + dir*t (t defines point of intersection from start along direction)
																//Has same result as (1-u-v)*v0 + u*v1 + v*v2.
				newRay.dir = scaleVec(tri0.n, 2*dotProduct(V, tri0.n)); 					//2(V dot N)N	
			}
			else	
			{
				newRay.startPt = addVecs( scaleVec(r.startPt, 1-tuv1.x), scaleVec(endPt, tuv1.x) );	
				newRay.dir = scaleVec(tri1.n, 2*dotProduct(V, tri1.n)); 			
			}
			
			
			newRay.dir = normalize(addVecs( scaleVec(V, -1.0), newRay.dir )); 
			
			
			if (i > numFaces*2)		//If box face, get color of wall to set as light diffuse and specular colors.
			{
				lightDiffuse = normalize(componentMult(lightDiffuse, boxColors[(i-numFaces*2)/2]));	//i-numFaces*2 = tri index. i/2 gives face index.
				lightSpecular = lightDiffuse;		
			}
		  	else				//Else, get color from teapot.
			{
				lightDiffuse = normalize(componentMult(lightDiffuse, teapotMaterials[1]));	
				lightSpecular = lightDiffuse;	
			}
	
			return newRay;	
		}	
	
   	}	//end triangle loop
	
    return r;	
}	

//-------------------------------------------------------------------------
//Set ambient, diffuse, specular, and shininess of the teapot (or OBJ loaded). Called by parseMtl().
void setTeapotMaterial()
{
	float ambient[] = {teapotMaterials[0].x, teapotMaterials[0].y, teapotMaterials[0].z, 1.0};
	float diffuse[] = {teapotMaterials[1].x, teapotMaterials[1].y, teapotMaterials[1].z, 1.0};
        float specular[] = {teapotMaterials[2].x, teapotMaterials[2].y, teapotMaterials[2].z, 1.0};
	float shininess[] = {teapotShiny};

        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
}

//------------------------------------------------------------------
//Set the current material colors for the box. Changes according to which face is being drawn.
void setBoxMaterial(struct point color)
{
	float diffuse[] = {color.x, color.y, color.z, 1.0};
        float specular[] = {0.3, 0.3, 0.3, 1.0};
        float shininess[] = {25.0};

        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
}

//------------------------------------------------------------------
//Render the teapot (or OBJ loaded).
void drawTeapot()
{
	glPushMatrix();						//Save current state (teapot and box may have different transformations).
	glTranslatef(teaTrans.x, teaTrans.y, teaTrans.z);	//Translate teapot to bottom face of box and side of and back into scene.
	glScalef(teaScale, teaScale, teaScale);			//Scale down original mesh.
	glRotatef(teaRotateY, 0.0, 1.0, 0.0);			//Rotate about y-axis. angle, x, y, z
	glRotatef(teaRotateX, 1.0, 0.0, 0.0);			//Rotate about x-axis.
	glRotatef(teaRotateZ, 0.0, 0.0, 1.0);			//Rotate about z-axis.	
	setTeapotMaterial();
	glUseProgram(teapotShaderProg);	

	GLuint ind_vert = glGetAttribLocation(teapotShaderProg, "vPosition");
	glBindBuffer(GL_ARRAY_BUFFER, 1);
	glBufferData(GL_ARRAY_BUFFER, 4*numFaces*3*sizeof(GLfloat), finalVertices, GL_STATIC_DRAW);	
	glVertexAttribPointer(ind_vert, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ind_vert);

	GLuint ind_norm = glGetAttribLocation(teapotShaderProg, "vNormal");
	glBindBuffer(GL_ARRAY_BUFFER, 2);
	glBufferData(GL_ARRAY_BUFFER, 4*numFaces*3*sizeof(GLfloat), finalNormals, GL_STATIC_DRAW);	
	glVertexAttribPointer(ind_norm, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ind_norm);

	GLuint ind_tc = glGetAttribLocation(teapotShaderProg, "normalTexCoord");
	glBindBuffer(GL_ARRAY_BUFFER, 3);
	glBufferData(GL_ARRAY_BUFFER, 4*numFaces*2*sizeof(GLfloat), finalTexCoords, GL_STATIC_DRAW);	
	glVertexAttribPointer(ind_tc, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ind_tc);

	GLuint ind_tan = glGetAttribLocation(teapotShaderProg, "tangent");
	glBindBuffer(GL_ARRAY_BUFFER, 4);
	glBufferData(GL_ARRAY_BUFFER, 4*numFaces*3*sizeof(GLfloat), finalTangents, GL_STATIC_DRAW);	
	glEnableVertexAttribArray(ind_tan);		
	glVertexAttribPointer(ind_tan, 3, GL_FLOAT, GL_FALSE, 0, NULL);			//attrib index, num of components, type, normalized, stride, pointer	

	GLuint ind_bitan = glGetAttribLocation(teapotShaderProg, "bitangent");
	glBindBuffer(GL_ARRAY_BUFFER, 5);
	glBufferData(GL_ARRAY_BUFFER, 4*numFaces*3*sizeof(GLfloat), finalBitangents, GL_STATIC_DRAW);	
	glEnableVertexAttribArray(ind_bitan);
	glVertexAttribPointer(ind_bitan, 3, GL_FLOAT, GL_FALSE, 0, NULL);	

	glActiveTexture(GL_TEXTURE0);		//Normal map.
	glBindTexture(GL_TEXTURE_2D, normalMap_ID);

	glDrawArrays(GL_QUADS, 0, numFaces*4); 	
	
	glPopMatrix();				//Return to pushed state.
}

//------------------------------------------------------------------
//Render the box face by face. row 0 = back, 1 = left, 2 = right, 3 = top, 4 = bottom, 5 = front
void drawBox()
{
	int i;
	glPushMatrix();			//Save current state.
	
	glActiveTexture(GL_TEXTURE1);	//Shadow map
	glBindTexture(GL_TEXTURE_2D, shadowMap_ID);

	setBoxMaterial(boxColors[0]);	//White
	glBegin(GL_QUADS);
	glNormal3f(boxNorms[3].x, boxNorms[3].y, boxNorms[3].z);
	for(i=0;i<4;i++) glVertex3f(boxVerts[3][i].x, boxVerts[3][i].y, boxVerts[3][i].z);	//Top
	glNormal3f(boxNorms[4].x, boxNorms[4].y, boxNorms[4].z);
	for(i=0;i<4;i++) glVertex3f(boxVerts[4][i].x, boxVerts[4][i].y, boxVerts[4][i].z);	//Bottom
	glNormal3f(boxNorms[0].x, boxNorms[0].y, boxNorms[0].z);
	for(i=0;i<4;i++) glVertex3f(boxVerts[0][i].x, boxVerts[0][i].y, boxVerts[0][i].z);	//Back
	glEnd();

	setBoxMaterial(boxColors[1]);	//Teal
	glNormal3f(boxNorms[1].x, boxNorms[1].y, boxNorms[1].z);
	glBegin(GL_QUADS);
	for(i=0;i<4;i++) glVertex3f(boxVerts[1][i].x, boxVerts[1][i].y, boxVerts[1][i].z);	//Left
	glEnd();

	setBoxMaterial(boxColors[2]);	//Purple
	glBegin(GL_QUADS);
	glNormal3f(boxNorms[2].x, boxNorms[2].y, boxNorms[2].z);
	for(i=0;i<4;i++) glVertex3f(boxVerts[2][i].x, boxVerts[2][i].y, boxVerts[2][i].z);	//Right
	glEnd();
		
	glPopMatrix();			//Return to pushed state.
}

//------------------------------------------------------------------
//Render the light source quad separately (with a different shader program set).
void drawLightSrc()
{  
	int i;
	glBegin(GL_QUADS);
	glNormal3f(0.0,-1.0,0.0);
	for(i=0;i<4;i++) glVertex3f(lightSrc[i].x,lightSrc[i].y,lightSrc[i].z);
	glEnd();
}

//------------------------------------------------------------------
//Save the transformation matrix when rendering from the light source. Used for getting shadowmap texture coordinates.
void saveGlMatrix(struct point eyePt, struct point viewPt)
{
	struct point upVec;

	glMatrixMode(GL_TEXTURE);		//Put into texture matrix for shader to use.
	glActiveTexture(GL_TEXTURE2); 		//Texture unit 3 used as shadowmap transformation matrix in shaders.
	glLoadIdentity();
	glTranslatef(0.0,0.0,-0.005);		//Prevents objects receiving shadow that are actually in light.	A fudge factor when comparing Zs.
	glScalef(0.5, 0.5, 0.5);		//Scaling and translating to put in range [-1, 1].
	glTranslatef(1.0, 1.0, 1.0);		
	gluPerspective(45.0, (float)(DM_W)/(float)(DM_H), 0.5, 20.0);
	
	upVec = getUpVec(eyePt, viewPt);
        gluLookAt(eyePt.x,eyePt.y,eyePt.z,viewPt.x,viewPt.y,viewPt.z,upVec.x,upVec.y,upVec.z);
}

//------------------------------------------------------------------
//Set the uniform variables in the shaders. Do not use the normal map in the box shader.
void setUniformParams(GLuint prog, int isBox)
{
	int loc;
	
	if (!isBox)
	{	
		loc = glGetUniformLocation(prog, "normalMap");
		glUniform1i(loc, 0);			//Texture *unit* 0 (GL_TEXTURE0).
	}
	else
	{
		loc = glGetUniformLocation(prog, "shadowMap");
		glUniform1i(loc, 1);				//Texture unit 1.
	}
}

//------------------------------------------------------------------
//Iterating through cast rays and their reflections, accumulate rendered scenes for each light generated.
void renderScene()
{
	struct point pt, dir, shadowEye, shadowView;
	struct ray r;
	int i, j, N, reflections;				//N = "Number of particles to start off the light source"
	float weight, randX, randZ;
	N = 20;
	reflections = 6;
	weight = (1.0 / (N*(reflections+1))) *1.5; 		//Weigh each accumulation a bit more than usual to compensate for darkness. (Dark areas not in light add up.)

	glClear(GL_ACCUM_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (i=0; i<N; i++)
	{
		lightDiffuse.x = 1.0;				//Each ray from light source starts out with the default diffuse and specular colors.
		lightDiffuse.y = 1.0;
		lightDiffuse.z = 1.0;
		lightSpecular.x = 1.0;			
		lightSpecular.y = 1.0;
		lightSpecular.z = 1.0;

		randX = ((rand() % 4000)-2000) * 0.0001;	//Generates random value between -0.2 and 0.2.
		randZ = ((rand() % 4000)-2000) * 0.0001;		
		pt.x = randX;					//Set random starting pt on light src.
		pt.y = lightSrc[0].y;
		pt.z = randZ;
		dir = getDirection(i);				//Generate direction using the Halton Sequence.	

		r.startPt = pt;
		r.dir = dir;	
		
		for (j=0; j<reflections+1; j++)			//+1 so there are initial calculations for original ray shot out.
		{
		//Draw scene to create shadowmap. (Currently seems to not correctly get into the shadowmap GL_TEXTURE_2D although does render from light location and direction.)
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);	

			glBindFramebuffer(GL_FRAMEBUFFER, 1);		
			glUseProgram(0);
			shadowEye = r.startPt;
			shadowView = addVecs(r.startPt, scaleVec(r.dir, 3.0));			//Scale by 3 to extend beyond box.
			setupViewVolume(DM_W, DM_H, scaleVec(shadowEye, 0.9), shadowView);
			setLights(origLightDiffuse, origLightSpecular, shadowEye);		//Set lights after the view volume so eye coords are calculated for shader.
			saveGlMatrix(shadowEye, shadowView);					//Save the current matrix (view from eye) transformation) for the shader to use.
			drawTeapot();	
			drawBox();							
			glFlush();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		//Draw scene normally.	
			view.x = JITTER*genrand();  view.y = JITTER*genrand();  view.z = JITTER*genrand();
			setupViewVolume(WINW, WINH, eye, view);					//Reset transformation matrix for rendering the geometry (vs. the shadowmap).	
			setLights(lightDiffuse, lightSpecular, r.startPt);			//lightDiffuse and lightSpecular had been changed in castRay().

			setUniformParams(boxShaderProg, 1);					//1 for isBox
			setUniformParams(teapotShaderProg, 0);

			glUseProgram(lightSrcShaderProg);	
			drawLightSrc();
			glUseProgram(boxShaderProg);	
			drawBox();
			glUseProgram(teapotShaderProg);	
			drawTeapot();

			r = castRay(r); 				//Returns a new ray that is the reflection at the intersection. Also updates the light colors.

			glFlush();
			glAccum(GL_ACCUM, weight);	
		}  //end reflections loop
	}  //end N loop 

	glAccum(GL_RETURN, 1.0);
}

//-------------------------------------------------------------------------
//Exit function called through glutKeyboardFunc().
void quit(unsigned char key, int x, int y)
{
        switch(key)
        {
                case 'q':
       			glDeleteBuffers(0); 
			free(finalVertices);
			free(finalNormals);
			free(finalTexCoords);
			free(finalTangents);	
			free(finalBitangents);
			free(faceVerts);	
                        exit(1);
                default:
                        break;
        }
}

//------------------------------------------------------------------
int main(int argc, char **argv)
{
        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_ACCUM);	//Do not set GL_DOUBLE (double-buffering) when using accum buffer. Accum might use wrong buffer then. Instead, use a singly-buffered visual. (Not all machines may allow this though.)
	
        glutInitWindowSize(WINW,WINH);
        glutInitWindowPosition(100,100);
        glutCreateWindow("The Lachrymosity of a Teapot");
	glClearAccum(0.0, 0.0, 0.0, 0.0);		
        glClearColor(0.32, 0.3, 0.32, 1.0);             //Medium gray slightly purple
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_VERTEX_ARRAY);	
	glEnableClientState(GL_NORMAL_ARRAY);	
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);	
	
	getBoxTris();	
	parseObj(argv[1]);				//Parse obj file passed in.
	buildShadowmap();				//Call once to init texture used for shadowmap.
	setupViewVolume(WINW, WINH, eye, view);		//Set up initially with actual eye and view points. (Later this is also called for generating shadowmap.)
	
        boxShaderProg = setShaders("box.vert", "box.frag");
        teapotShaderProg = setShaders("teapot.vert", "teapot.frag");
        lightSrcShaderProg = setShaders("lightSrc.vert", "lightSrc.frag");
        setUniformParams(boxShaderProg, 1);
        setUniformParams(teapotShaderProg, 0);

	glutDisplayFunc(renderScene);          	 	//Looping executions of renderSene()
        glutKeyboardFunc(quit);                		//Keyboard actions go to quit().
        glutMainLoop();
        return 0;
}

