// Jessica Baron        Spring 2017     CPSC 6050       Project 3
uniform sampler2D normalMap;
in vec3 eyeCoord_vPos, eyeCoord_vNormal, eyeCoord_vTangent, eyeCoord_vBitangent;
in vec2 texCoords;

void main()
{
        vec3 P, N, L, V, H;
        vec4 lightDiff, lightSpec;
        vec4 diffuse = gl_FrontMaterial.diffuse;   
        vec4 specular = gl_FrontMaterial.specular;  
	vec4 projectedCoords;
        vec4 shadowClarity = vec4(1.0, 1.0, 1.0, 0.0);
        vec4 amb = vec4(0.1, 0.1, 0.1, 0.0);
        float shadowDepth;
        float gamma;
        float shininess = gl_FrontMaterial.shininess;
        float pi = 3.14159265;
        float normFactor = (shininess+2.0)/(pi*8.0);    //Phong normalizing factor 
	mat3 transf;
	vec3 mapNormal;

//Calculate diffuse and specular for the key light.
        P = eyeCoord_vPos;
        L = normalize(gl_LightSource[0].position - P); 
        V = normalize(-P);                         
        H = normalize(L+V);                          

     //3x3 transform matrix to calculate new normal from normal map xyz coord.
	transf = mat3(eyeCoord_vTangent, eyeCoord_vBitangent, eyeCoord_vNormal);
	mapNormal = vec3( texture2D(normalMap, texCoords.st) );
	N = normalize(transf * normalize(mapNormal));	
        
        lightDiff = max(dot(N, L), 0.0) * gl_LightSource[0].diffuse;
        lightSpec = normFactor * pow(max(dot(H, N), 0.0), shininess) *gl_LightSource[0].specular;

        gl_FragColor = specular*lightSpec + diffuse*lightDiff;
}

