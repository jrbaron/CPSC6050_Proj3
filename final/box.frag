// Jessica Baron        Spring 2017     CPSC 6050       Project 3
uniform sampler2D shadowMap;
in vec3 eyeCoord_vNormal, eyeCoord_vPos;          
in vec4 shadowMapCoords;

void main()
{
        vec3 P, N, L, V, H;
        vec4 lightDiff, lightSpec;
        vec4 diffuse = gl_FrontMaterial.diffuse;       
        vec4 specular = gl_FrontMaterial.specular;    
	vec4 projectedCoords;
        vec4 shadowClarity = vec4(1.0, 1.0, 1.0, 0.0);
        float shadowDepth;
        float gamma;
        float shininess = gl_FrontMaterial.shininess;
        float pi = 3.14159265;
        float normFactor = (shininess+2.0)/(pi*8.0);    //Phong normalizing factor for specular shading.
	mat3 transf;
	vec3 mapNormal;

//Calculate diffuse and specular for the key light.
        P = eyeCoord_vPos;
        L = normalize(gl_LightSource[0].position - P); 
        V = normalize(-P);                              //View vec = camera pos - view point.
        H = normalize(L+V);                             //Halfway vector between light and view unit vectors.
        N = normalize(eyeCoord_vNormal);  

        lightDiff = max(dot(N, L), 0.0) *gl_LightSource[0].diffuse;
        lightSpec = normFactor * pow(max(dot(H, N), 0.0), shininess) *gl_LightSource[0].specular;

        projectedCoords = shadowMapCoords/shadowMapCoords.w;          
        shadowDepth = texture2D(shadowMap, projectedCoords.st);				//s,t coords are in range [-1,1].
        if(shadowDepth < projectedCoords.z) shadowClarity = vec4(0.7,0.7,0.7,0.0);      //Fade shadow.

        gl_FragColor = (specular*lightSpec + diffuse*lightDiff) *shadowClarity;
}

