// Jessica Baron        Spring 2017     CPSC 6050       Project 3
attribute vec4 vPosition;
attribute vec3 vNormal, tangent, bitangent;
attribute vec2 normalTexCoord;
out vec3 eyeCoord_vPos, eyeCoord_vNormal, eyeCoord_vTangent, eyeCoord_vBitangent;
out vec2 texCoords;

void main()
{
        eyeCoord_vPos = gl_ModelViewMatrix * vPosition;
        eyeCoord_vNormal = gl_NormalMatrix * vNormal; 
	eyeCoord_vTangent = gl_NormalMatrix * tangent;
	eyeCoord_vBitangent = gl_NormalMatrix * bitangent; 
	texCoords = vec2(normalTexCoord.x, normalTexCoord.y);	
        gl_Position = gl_ProjectionMatrix*gl_ModelViewMatrix*vPosition;  
}
