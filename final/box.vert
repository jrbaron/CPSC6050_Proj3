// Jessica Baron        Spring 2017     CPSC 6050       Project 3
out vec3 eyeCoord_vNormal, eyeCoord_vPos;                   		
out vec4 shadowMapCoords;

void main()
{
        eyeCoord_vNormal = gl_NormalMatrix * gl_Normal;       
        eyeCoord_vPos = gl_ModelViewMatrix * gl_Vertex;
        gl_Position = gl_ProjectionMatrix*gl_ModelViewMatrix*gl_Vertex;  
        shadowMapCoords = gl_TextureMatrix[2] * gl_Vertex;                      //Shadowmap transformation matrix.
}
